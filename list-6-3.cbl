       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING6-3 .
       AUTHOR. Nithiphat.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  REP-COUNT         PIC 9(4).
       01  PRN-REP-COUNT     PIC Z,ZZ9.
       01  NUM-OF-TIMES   PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM VARYING REP-COUNT FROM 0 BY 50
              UNTIL REP-COUNT = NUM-OF-TIMES 
              MOVE REP-COUNT TO PRN-REP-COUNT 
              DISPLAY "COUNTING " PRN-REP-COUNT 
           END-PERFORM
           MOVE REP-COUNT TO PRN-REP-COUNT 
           DISPLAY "IF I have Told You Once."
           DISPLAY "I've Told You " PRN-REP-COUNT " Times."
           GOBACK
       .
