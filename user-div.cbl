       IDENTIFICATION DIVISION. 
       PROGRAM-ID. USER-DIV.
       AUTHOR. Nithiphat.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM1     PIC 9(5)       VALUE 0.
       01  NUM2     PIC 9(5)       VALUE 0.
           88 NUM2ISZERO VALUE 0.
       01  RESULT   PIC 9(5)V9(3)  VALUE 0.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-USER-DIV THRU 001-EXIT
           GOBACK. 

       001-USER-DIV.
           DISPLAY "input NUM1: " WITH NO ADVANCING 
           ACCEPT NUM1 
           DISPLAY "input NUM2: " WITH NO ADVANCING 
           ACCEPT NUM2 
           IF NUM2ISZERO  THEN
              DISPLAY "Error: NUM2 IS ZERO."
              GO TO 001-EXIT 
           END-IF 
           COMPUTE RESULT  = NUM1  / NUM2 
           DISPLAY "RESULT: " RESULT  
           .

       001-User-Display-End.
           DISPLAY "END OF USER-DIV".

       001-EXIT.
           EXIT .
