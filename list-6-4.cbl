       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LIST6-4 .
       AUTHOR. Nithiphat.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  COUNTERS.
           02 HUNDREDSCOUNT     PIC 99   VALUE ZEROS .
           02 TENSCOUNT         PIC 99   VALUE ZEROS .
           02 UNITSCOUNT        PIC 99   VALUE ZEROS .

       01  ODOMETER.
           02 PRNHUNDREDS       PIC 9.
           02 FILLER            PIC X VALUE "-".
           02 PRNTENS           PIC 9.
           02 FILLER            PIC X VALUE "-".
           02 PRNUNITS         PIC 9.

       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "USING AN OUT-OF-LINE PERFORM"
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT 
               VARYING HUNDREDSCOUNT  FROM 0 BY 1 UNTIL HUNDREDSCOUNT  
               > 9
               AFTER TENSCOUNT FROM 0 BY 1 UNTIL TENSCOUNT > 9
               AFTER UNITSCOUNT FROM 0 BY 1 UNTIL UNITSCOUNT > 9
           GOBACK
           .
       001-COUNT-MILEAGE.
           MOVE HUNDREDSCOUNT  TO PRNHUNDREDS  
           MOVE TENSCOUNT      TO PRNTENS  
           MOVE UNITSCOUNT     TO PRNUNITS  
           DISPLAY "Out - " ODOMETER  
           .
       001-EXIT.
           EXIT .
